package nl.codecentric.tdd.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

public class MasterMindTest {

    private MasterMind masterMind;

    @BeforeEach
    void setUp() {
        masterMind = new MasterMind();
    }

    @Test
    void allInCorrect() {
        List<Integer> result = masterMind.solve(Arrays.asList("red"), Arrays.asList("blue"));

        assertThat(result).isEqualTo(Arrays.asList(0,0));
    }
}
